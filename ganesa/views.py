import json

from django.shortcuts import render
from django.template.loader import render_to_string
from django.views.decorators.csrf import csrf_exempt

from ganesa.models import Device, Sensor, DeviceLogListeners, DeviceDataLog
from ganesa.models import SensorData


def dashboard(request):
    devices = Device.objects.all()
    dataCount = SensorData.objects.all().count()

    context = {
        'devices': devices,
        'dataCount': dataCount
    }
    return render(request, 'ganesa/pages/dashboard.html', context)


def device(request, device_id):
    try:
        device = Device.objects.get(pk=device_id)
    except Device.DoesNotExist:
        return render(request, 'ganesa/pages/dashboard.html', {'devices': Device.objects.all()})

    context = {
        'device': device,
        'devices': Device.objects.all()
    }
    return render(request, 'ganesa/pages/device.html', context)


def additional_device(request, device_id):
    try:
        device = Device.objects.get(pk=device_id)
    except Device.DoesNotExist:
        return render(request, 'ganesa/pages/dashboard.html', {'devices': Device.objects.all()})
    device_sensors = device.sensors.all()

    test = {
        "name": "a",
        "datas": [
            {"a": 1, "b": 2},
            {"a": 1, "b": 3}
        ]
    }

    device_sensors_data = {}
    sensor_names = []
    for device_sensor in device_sensors:
        sensor_names.append(device_sensor.get_chart_name())
        device_sensors_data.update({
            device_sensor.get_chart_name(): []
        })
        device_sensor_data_all = SensorData.objects.filter(device_id=device_id, sensor=device_sensor)
        for item in device_sensor_data_all:
            device_sensors_data[device_sensor.get_chart_name()].append({
                "data": json.loads(item.data.replace('\'', '"')),
                "meta": json.loads(item.meta_data.replace('\'', '"'))
            })

    json_sensors_data = json.dumps(device_sensors_data)

    context = {
        'device': device,
        'devices': Device.objects.all(),
        'device_data': json_sensors_data,
        'sensor_names': sensor_names
    }
    return render(request, 'ganesa/pages/additional_device.html', context)


def logs_list(request, device_id):
    try:
        device = Device.objects.get(pk=device_id)
    except Device.DoesNotExist:
        return render(request, 'ganesa/pages/dashboard.html', {'devices': Device.objects.all()})
    device_sensors = device.sensors.all()
    device_sensors_count = device_sensors.count()

    device_logs = DeviceDataLog.objects.filter(device=device)
    device_total_logs_count = device_logs.count()

    inc_logs_count = device_logs.filter(log_type='INC', action='captured').count()
    dec_logs_count = device_logs.filter(log_type='DEC', action='captured').count()
    eq_logs_count = device_logs.filter(log_type='EQ', action='captured').count()


    context = {
        'device_sensors_count': device_sensors_count,
        'device_total_logs_count': device_total_logs_count,
        'inc_logs_count': inc_logs_count,
        'dec_logs_count': dec_logs_count,
        'eq_logs_count': eq_logs_count,
        'device_logs': device_logs
    }
    return render(request, 'ganesa/pages/logs_list.html', context)

def log_view(request, log_id):
    try:
        log = DeviceDataLog.objects.get(pk=log_id)
    except DeviceDataLog.DoesNotExist:
        return render(request, 'ganesa/pages/dashboard.html', {'devices': Device.objects.all()})

    return render(request, 'ganesa/pages/logs_view.html', {'log': log})


def device_settings(request, device_id):
    try:
        device = Device.objects.get(pk=device_id)
    except Device.DoesNotExist:
        return render(request, 'ganesa/pages/dashboard.html', {'devices': Device.objects.all()})

    context = {
        'device': device,
        'devices': Device.objects.all()
    }
    return render(request, 'ganesa/pages/device_settings.html', context)


@csrf_exempt
def device_sensor_settings(request, device_id, sensor_id):
    try:
        device = Device.objects.get(pk=device_id)
    except Device.DoesNotExist:
        return render(request, 'ganesa/pages/dashboard.html', {'devices': Device.objects.all()})

    try:
        sensor = Sensor.objects.get(pk=sensor_id)
    except Sensor.DoesNotExist:
        return render(request, 'ganesa/pages/dashboard.html', {'devices': Device.objects.all()})

    setting = DeviceLogListeners.objects.filter(device_id=device_id, sensor_id=sensor_id)
    if len(setting) > 0:
        setting = setting[0]
    else:
        setting = False

    if request.method == 'GET':
        if setting:
            setting_content = {
                'setting': json.loads(setting.data)
            }
            setting_data = render_to_string('ganesa/pages/forms/' + sensor.type + '.html', setting_content)
        else:
            setting_data = render_to_string('ganesa/pages/forms/' + sensor.type + '.html')

        context = {
            'device': device,
            'sensor': sensor,
            'type': sensor.type,
            'data': setting_data,
        }

        return render(request, 'ganesa/pages/device_sensor_settings.html', context)
    else:
        data = sensor.get_limit_data_json(request.POST)
        if setting:
            setting.data = json.dumps(data)
            setting.save()
        else:
            DeviceLogListeners.objects.create(device=device, sensor=sensor, data=json.dumps(data))

        context = {
            'device': device,
            'sensor': sensor
        }
        return render(request, 'ganesa/pages/device_sensor_settings.html', context)
