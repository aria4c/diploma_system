$(document).ready(function(){
    var socket = new WebSocket(
        'ws://' + window.location.host +
        '/listener/');
	socket.onmessage = websocket_msj_auth_received;

	$('#formulario').submit(function(e){
	});
});

function websocket_msj_auth_received(e) {
    data = JSON.parse(e.data);
    var message_type = data.message.message_type;
    var message_data = data.message.data;
    //console.log(message_data);
    switch (message_type){
        case "device_registered": {
            var message = "New device "+message_data.device_name+" registered";
            $.notify(message, 'success');

            $('#main-menu').append(
                `
                   <li id="menu-device-${message_data.device_id}">
                                        <a href="/device/${message_data.device_id}">
                                            <span id="menu-device-${message_data.device_id}-status" class="glyphicon glyphicon-record
                                            device-offline">

                                        </span>${message_data.device_name}</a>
                                    </li>
                `
            );


            break;
        }
        case "device_disconnected": {
            var message = "Device " + message_data.device_id + " get offline.";
            $.notify(message, 'warn');
            $('#device-status').removeClass('device-online');
            $('#device-status').html("Offline");
            $('#device-status').addClass('device-offline');

            $('#menu-device-'+message_data.device_id+'-status').removeClass('device-online');
            $('#menu-device-'+message_data.device_id+'-status').addClass('device-offline');
            break;
        }
        case "device_streaming_start": {
            var message = "Device " + message_data.device_id + " back online.";
            $.notify(message, 'info');
            $('#device-status').removeClass('device-offline');
            $('#device-status').html("Online");
            $('#device-status').addClass('device-online');
            $('#menu-device-'+message_data.device_id+'-status').removeClass('device-offline');
            $('#menu-device-'+message_data.device_id+'-status').addClass('device-online');
            break;
        }
        case "device_limit": {
            console.log(message_data);
            var message = "Device " + message_data.device_name + "#" + message_data.device_id;
            if(message_data.action === 0) {
                message += " back to normal.";
                $.notify(message, 'info');
            } else {
                message += " exceeded the limit.";
                $.notify(message, 'warn');
            }
            break;
        }

    }
}
