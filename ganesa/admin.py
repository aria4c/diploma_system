from django.contrib import admin
from ganesa.models import *
# Register your models here.
admin.site.register(Sensor)
admin.site.register(Device)
admin.site.register(SensorData)
