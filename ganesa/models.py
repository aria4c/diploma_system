import json
from datetime import datetime

from asgiref.sync import async_to_sync
from django.db import models

# Create your models here.
#   id  sensor  device  values  meta
#   int fk      fk      json    json
#



class Sensor(models.Model):
    name = models.CharField(max_length=16)
    type = models.CharField(max_length=50, default='XYZ')

    # chart_type = models.CharField(max_length=100)

    def __str__(self):
        return self.name

    def get_chart_name(self):
        return self.name.replace(' ', '-').lower()

    def get_limit_data_json(self, array):
        if (self.type == 'TEMP'):
            limit = array['limit-c']
            return {'c': limit}
        elif (self.type == 'XYZ'):
            result = {}
            xlimit = array.get('limit-x')
            xbeh = array.get('limit-x-behavior')
            if xlimit:
                result['X'] = {
                    "limit": xlimit,
                    "behavior": xbeh,
                    "flag": 0
                }

            ylimit = array.get('limit-y')
            ybeh = array.get('limit-y-behavior')
            if ylimit:
                result['Y'] = {
                    "limit": ylimit,
                    "behavior": ybeh,
                    "flag": 0
                }

            zlimit = array.get('limit-z')
            zbeh = array.get('limit-z-behavior')
            if zlimit:
                result['Z'] = {
                    "limit": zlimit,
                    "behavior": zbeh,
                    "flag": 0
                }

            return result


class Device(models.Model):
    device_id = models.CharField(primary_key=True, max_length=36)
    name = models.CharField(max_length=30, null=True)
    sensors = models.ManyToManyField(Sensor)
    is_authenticated = models.BooleanField
    is_online = models.BooleanField(default=False)

    def __str__(self):
        return self.name


class SensorData(models.Model):
    device_id = models.ForeignKey(Device, on_delete=models.CASCADE, null=True, default=None)
    sensor = models.ForeignKey(Sensor, on_delete=models.CASCADE)
    meta_data = models.CharField(max_length=10000, null=True)
    data = models.CharField(max_length=10000, null=True)


class DeviceDataLog(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    comment = models.CharField(max_length=500, null=True)
    device = models.ForeignKey(Device, on_delete=models.CASCADE, null=False, default=None)
    sensor = models.ForeignKey(Sensor, on_delete=models.CASCADE)
    values = models.CharField(max_length=1000, null=True)
    log_type = models.CharField(max_length=15, null=True)
    action = models.CharField(max_length=15, null=True)


class DeviceLogListeners(models.Model):
    device = models.ForeignKey(Device, on_delete=models.CASCADE, null=False, default=None)
    sensor = models.ForeignKey(Sensor, on_delete=models.CASCADE)
    data = models.CharField(max_length=10000, null=True)

    def check_limits(self, data):
        type = self.sensor.type

        if type == 'XYZ':
            dataJson = data
            listenerData = json.loads(self.data)

            for field, field_data in listenerData.items():
                if field_data['behavior'] == 'EQ':
                    if dataJson[field] == float(field_data['limit'] && field_data['flag'] == 0):
                        dataLog = DeviceDataLog.objects.create(device=self.device, sensor=self.sensor);
                        Comment = "Девайс " + str(self.device.name) + " стал равен лимиту " + str(
                            field_data['limit']) + " для значения " + str(field)
                        dataLog.comment = Comment
                        dataLog.created_at = datetime.now()
                        dataLog.log_type = "EQ"
                        dataLog.values = json.dumps(dataJson)
                        dataLog.action = "captured"
                        dataLog.save()

                        print("Data: " + str(dataJson[field]) + " LIMIT: " + field_data['limit'])
                        field_data['flag'] = 1
                        listenerData[field] = field_data
                        self.data = json.dumps(listenerData)
                        self.save()
                        return {
                            'device_id': self.device.device_id,
                            'device_name': self.device.name,
                            'type': field_data['behavior'],
                            'action': field_data['flag']
                        }

                    if dataJson[field] < float(field_data['limit']) and field_data['flag'] == 1:
                        dataLog = DeviceDataLog.objects.create(device=self.device, sensor=self.sensor);
                        Comment = "Девайс " + str(self.device.name) + " вернулся в норму по " + str(
                            field_data['limit']) + " для значения " + str(field)
                        dataLog.comment = Comment
                        dataLog.created_at = datetime.now()
                        dataLog.log_type = "eq"
                        dataLog.action = "released"
                        dataLog.values = json.dumps(dataJson)
                        dataLog.save()

                        print("Data: " + str(dataJson[field]) + " LIMIT: " + field_data['limit'])
                        field_data['flag'] = 0
                        listenerData[field] = field_data
                        self.data = json.dumps(listenerData)
                        self.save()
                        return {
                            'device_id': self.device.device_id,
                            'device_name': self.device.name,
                            'type': field_data['behavior'],
                            'action': field_data['flag']
                        }
                elif field_data['behavior'] == 'INC':
                    if dataJson[field] > float(field_data['limit']) and field_data['flag'] == 0:
                        dataLog = DeviceDataLog.objects.create(device=self.device, sensor=self.sensor);
                        Comment = "Девайс "+str(self.device.name)+" Преодолел лимит "+str(field_data['limit']) + " для значения "+str(field);
                        dataLog.comment = Comment
                        dataLog.created_at = datetime.now()
                        dataLog.log_type = "INC"
                        dataLog.values = json.dumps(dataJson)
                        dataLog.action = "captured"
                        dataLog.save()

                        print("Data: " + str(dataJson[field]) + " LIMIT: " + field_data['limit'])
                        field_data['flag'] = 1
                        listenerData[field] = field_data
                        self.data = json.dumps(listenerData)
                        self.save()
                        return {
                            'device_id': self.device.device_id,
                            'device_name': self.device.name,
                            'type': field_data['behavior'],
                            'action': field_data['flag']
                        }
                    if dataJson[field] < float(field_data['limit']) and field_data['flag'] == 1:
                        dataLog = DeviceDataLog.objects.create(device=self.device, sensor=self.sensor);
                        Comment = "Девайс " + str(self.device.name) + " вернулся в норму по " + str(
                            field_data['limit']) + " для значения " + str(field);
                        dataLog.comment = Comment
                        dataLog.created_at = datetime.now()
                        dataLog.log_type = "INC"
                        dataLog.action = "released"
                        dataLog.values = json.dumps(dataJson)
                        dataLog.save()

                        print("Data: " + str(dataJson[field]) + " LIMIT: " + field_data['limit'])
                        field_data['flag'] = 0
                        listenerData[field] = field_data
                        self.data = json.dumps(listenerData)
                        self.save()
                        return {
                            'device_id': self.device.device_id,
                            'device_name': self.device.name,
                            'type': field_data['behavior'],
                            'action': field_data['flag']
                        }

                elif field_data['behavior'] == 'DEC':
                    if dataJson[field] < float(field_data['limit']) and field_data['flag'] == 0:
                        dataLog = DeviceDataLog.objects.create(device=self.device, sensor=self.sensor);
                        Comment = "Девайс "+str(self.device.name)+" опустился ниже лимита "+str(field_data['limit']) + " для значения "+str(field);
                        dataLog.comment = Comment
                        dataLog.created_at = datetime.now()
                        dataLog.log_type = "dec"
                        dataLog.values = json.dumps(dataJson)
                        dataLog.action = "captured"
                        dataLog.save()

                        print("Data: " + str(dataJson[field]) + " LIMIT: " + field_data['limit'])
                        field_data['flag'] = 1
                        listenerData[field] = field_data
                        self.data = json.dumps(listenerData)
                        self.save()
                        return {
                            'device_id': self.device.device_id,
                            'device_name': self.device.name,
                            'type': field_data['behavior'],
                            'action': field_data['flag']
                        }
                    if dataJson[field] > float(field_data['limit']) and field_data['flag'] == 1:
                        dataLog = DeviceDataLog.objects.create(device=self.device, sensor=self.sensor);
                        Comment = "Девайс " + str(self.device.name) + " вернулся в норму по " + str(
                            field_data['limit']) + " для значения " + str(field);
                        dataLog.comment = Comment
                        dataLog.created_at = datetime.now()
                        dataLog.log_type = "вус"
                        dataLog.action = "released"
                        dataLog.values = json.dumps(dataJson)
                        dataLog.save()

                        print("Data: " + str(dataJson[field]) + " LIMIT: " + field_data['limit'])
                        field_data['flag'] = 0
                        listenerData[field] = field_data
                        self.data = json.dumps(listenerData)
                        self.save()
                        return {
                            'device_id': self.device.device_id,
                            'device_name': self.device.name,
                            'type': field_data['behavior'],
                            'action': field_data['flag']
                        }


        elif type == 'TEMP':
            a = 1


        return
