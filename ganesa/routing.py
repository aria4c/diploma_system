from django.conf.urls import url

from . import consumers

websocket_urlpatterns = [
    url(r'auth/', consumers.AuthConsumer),
    url(r'^data/(?P<device_id>[^/]+)', consumers.StreamingDevice),
    url(r'listener/', consumers.ListenerConsumer)
]
